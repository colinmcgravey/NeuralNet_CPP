//
//  model.cpp
//  matrix
//
//  Created by Colin McGravey on 8/9/22.
//

#include "model.hpp"
#include "dense.hpp"
#include "RELu.hpp"
#include <stdio.h>
#include <iostream>
#include <vector>
#include <cmath>


double Model::mse_loss(Matrix &targets, Matrix &outputs) {
    
    loss_gradient = Matrix_init(1, num_outputs, false); 
    
    vector<double> losses;

    for (int i = 0; i < targets.rows; i++) {
        
        double sum = 0;
        
        for (int j = 0; j < targets.cols; j++) {
            
            double diff = targets.data[i][j] - outputs.data[i][j];
            sum += pow(diff, 2);
            double loss_grad = (2 * (outputs.data[i][j] - targets.data[i][j])) / targets.cols;
            loss_gradient.data[i][j] = loss_grad;
            
            
        }
        losses.push_back(sum / targets.cols);
        
    }
    
    double avg = 0;
    for (int i = 0; i < losses.size(); i++) {
        avg += losses[i];
    }
    
    return avg / losses.size();
}

double Model::fit(Matrix inputs, Matrix targets, double lr) {
    
    Matrix outputs;
    Matrix prev_grads;
    for (int i = 0; i < model.size(); i++) {
        outputs = model[i]->forward_prop(inputs);
        inputs = outputs;
    }
    
    double loss = mse_loss(targets, outputs);
    
    for (size_t j = model.size() - 1; j >= 0; j--) {
        prev_grads = model[j]->back_prop(loss_gradient, lr);
        loss_gradient = prev_grads;
        if (j == 0) { break; }
    }
    
    return loss; 
    
}


double Model::epoch_end(vector<double> epoch_loss) {
    double sum = 0.0;
    for (size_t i = 0; i < epoch_loss.size(); i++) {
        sum += epoch_loss[i];
    }
    return sum / epoch_loss.size();
}

void Model::add(string layer, size_t in, size_t out) {
    if (layer == "Dense") {
        model.push_back(new Dense(in, out));
    }
    if (layer == "RELu") {
        model.push_back(new RELu(in, out)); 
    }
}

void Model::summary() {
    for (int i = 0; i < model.size(); i++) {
        model[i]->view_params();
        std::cout << "\n"; 
    }
}
