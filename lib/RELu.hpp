//
//  RELu.hpp
//  matrix
//
//  Created by Colin McGravey on 8/10/22.
//

#ifndef RELu_hpp
#define RELu_hpp

#include <stdio.h>
#include <iostream>
#include <vector>
#include <cmath>
#include "Layer.h"
#include "matrix.hpp"
using namespace std;

class RELu : public Layer {
    
public:
    
    RELu(size_t n, size_t m) : Layer(n, m) {
        lyr_output = Matrix_init(1, m, false);
        lyr_input = Matrix_init(1, n, false); 
    }
    
    void print_outs();
    
    
    void view_params();
    
    
    Matrix forward_prop(Matrix &inputs);
    
    
    Matrix back_prop(Matrix &loss_grads, double lr); 
    
    
    
private:
    
    Matrix lyr_output;
    Matrix lyr_input;
    
    
};

#endif /* RELu_hpp */
