//
//  RELu.cpp
//  matrix
//
//  Created by Colin McGravey on 8/10/22.
//

#include "RELu.hpp"
#include <iostream>
#include <vector>
#include <cmath>
#include "matrix.hpp"
using namespace std;


void RELu::print_outs() {
    for (int i = 0; i < lyr_output.rows; i++) {
        for (int j = 0; j < lyr_output.cols; j++) {
            std::cout << "[" << lyr_output.data[i][j] << "]";
        }
        std::cout << "\n";
    }
}

void RELu::view_params() {
    std::cout << "RELu activation layer" << "\n";
}

Matrix RELu::forward_prop(Matrix &inputs) {
    lyr_input = inputs;
    for (int i = 0; i < lyr_input.rows; i++) {
        for (int j = 0; j < lyr_input.cols; j++) {
            lyr_output.data[i][j] = (std::max(0.0, lyr_input.data[i][j]));
        }
    }
    return lyr_output;
}

Matrix RELu::back_prop(Matrix &loss_grads, double lr) {
    
    Matrix x_grads = Matrix_init(1, loss_grads.cols, false);
    
    for (int i = 0; i < loss_grads.rows; i++) {
        
        for (int j = 0; j < loss_grads.cols; j++) {
            
            int deriv;
            if (lyr_input.data[i][j] > 0) { deriv = 1; }
            else { deriv = 0; }
            double prod = loss_grads.data[i][j] * deriv;
            x_grads.data[i][j] = prod;
        }
        
    }
    
    return x_grads; 
}
