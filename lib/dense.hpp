//
//  dense.hpp
//  matrix
//
//  Created by Colin McGravey on 8/9/22.
//

#ifndef dense_hpp
#define dense_hpp

#include <stdio.h>
#include <iostream>
#include <vector>
#include <cmath>
#include "matrix.hpp"
#include "Layer.h"
using namespace std;


// Dense Layer Class, derived from Layer Class 
class Dense : public Layer {
    
public:
    
    // constructor for the Dense layer, initalizes weights, biases, and i/o matrices
    Dense(size_t n, size_t m) : Layer(n, m) {
        weights = Matrix_init(n, m, true);
        biases = Matrix_init(1, m, true);
        lyr_input = Matrix_init(1, n, false);
        lyr_output = Matrix_init(1, m, false);
    }
    
    // prints the layer outputs
    void print_outs();
    
    // allows user to view all weights and biases of the layer
    void view_params();
    
    // propagates input forwar through the layer
    Matrix forward_prop(Matrix &inputs);
    
    // propagates loss gradients back to previous layer
    Matrix back_prop(Matrix &loss_grads, double lr); 
    
    
private:
    
    
    // a dense layer will have four matrices within it, weights, biases, and layer output and input for purpose of grad calculations
    Matrix weights;
    Matrix biases;
    Matrix lyr_output;
    Matrix lyr_input;
    
};


#endif /* dense_hpp */
