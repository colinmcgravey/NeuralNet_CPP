//
//  Layer.h
//  matrix
//
//  Created by Colin McGravey on 8/9/22.
//

#ifndef Layer_h
#define Layer_h

#include <iostream>
#include <vector>
#include <cmath>
#include "matrix.hpp"
using namespace std;

class Layer {
    
public:
    
    // constructs a layer
    Layer(size_t n, size_t m) : input_dims(n), output_dims(m) {}
    
    // prints layer output
    virtual void print_outs() { return; }
    
    // prints layer parameters
    virtual void view_params() { return; }
    
    // propagates input forward
    virtual Matrix forward_prop(Matrix &inputs) = 0;
    
    // propagates gradients backward
    virtual Matrix back_prop(Matrix &loss_grads, double lr) = 0; 
    
    
    
private:
    
    // serves to track layer dimensions
    size_t input_dims;
    size_t output_dims;
    
    
}; 

#endif /* Layer_h */
