//
//  model.hpp
//  matrix
//
//  Created by Colin McGravey on 8/9/22.
//

#ifndef model_hpp
#define model_hpp

#include <stdio.h>
#include <iostream>
#include <vector>
#include <cmath>
#include "Layer.h"
using namespace std;


class Model {
    
    
public:
    
    Model(size_t num) : num_outputs(num) {}
    
    
    double mse_loss(Matrix &targets, Matrix &outputs);
    
    
    double fit(Matrix inputs, Matrix targets, double lr);
    
    
    double epoch_end(vector<double> epoch_loss);
    
    void add(string layer, size_t in, size_t out);
    
    void summary(); 
    
    
    
    
private:
    
    vector<Layer *> model;
    
    
    Matrix loss_gradient;
    
    size_t num_outputs;
    
    
};

#endif /* model_hpp */
