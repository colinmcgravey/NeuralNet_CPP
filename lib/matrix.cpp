//
//  matrix.cpp
//  matrix
//
//  Created by Colin McGravey on 8/9/22.
//

#include "matrix.hpp"
#include <stdio.h>
#include <iostream>
#include <vector>
#include <cmath>
#include <algorithm>
using namespace std;

// used to create matrices for weights and biases in the nn
double random_double() {
    return double(rand()) / (double(RAND_MAX) + 1.0) - 0.5;
}

// initializes a Matrix with the specified height and width, if random flag is True, data values are random doubles
Matrix Matrix_init(size_t height, size_t width, bool random) {
    
    // creates new matrix and initalizes width and height
    Matrix temp;
    temp.cols = width;
    temp.rows = height;
    temp.data.resize(height);
    // inserts a 0 or random double in each data value dependent on random flag
    for (int i = 0; i < temp.rows; i++) {
        // if random is not flagged, simply resize the row vector to appropriate width
        if (!random) { temp.data[i].resize(width); }
        
        // if random is flagged, populate the row vector with random doubles and assign it to the data vector
        else {
            vector<double> row;
            for (int j = 0; j < temp.cols; j++) {
                row.push_back(random_double());

            }
            temp.data[i] = row;
        }
    }
    
    // returns new matrix
    return temp;
} // matrix_init()

// scalar multiplacation of a matrix
Matrix sc_multiply(double c, Matrix mat1) {
    
    // creates matrix to contain the product
    Matrix prod = Matrix_init(mat1.rows, mat1.cols, false);
    
    // loops through mat1 and performs element-wise multiply
    for (int i = 0; i < mat1.rows; i++) {
        for (int j = 0; j < mat1.cols; j++) {
            prod.data[i][j] = c * mat1.data[i][j];
        }
    }
    
    // returns product
    return prod;
} // sc_multiply()

// Matrix addition
Matrix matrix_add(Matrix mat1, Matrix mat2) {
    
    // ensure matrices are of similar sizes
    if (mat1.rows != mat2.rows || mat1.cols != mat2.cols) {
        std::cout << "Error in Matrix Addition: incompatible sizes" << "\n";
        exit(1);
    }
    
    // create matrix to contain the sum
    Matrix sum = Matrix_init(mat1.rows, mat1.cols, false);
    
    // loops through mat1 and mat2 and performs element-wise addition
    for (int i = 0; i < mat1.rows; i++) {
        for (int j = 0; j < mat2.cols; j++) {
            sum.data[i][j] = mat1.data[i][j] + mat2.data[i][j];
        }
    }
    
    // returns the sum
    return sum;
} // add()

// Matrix subtraction
Matrix matrix_subtract(Matrix mat1, Matrix mat2) {

    // ensure matrices are of similar sizes
    if (mat1.rows != mat2.rows || mat1.cols != mat2.cols) {
        std::cout << "Error in Matrix Subtraction: incompatible sizes" << "\n";
        exit(1);
    }
    
    // create matrix to contain difference
    Matrix difference = Matrix_init(mat1.rows, mat1.cols, false);
    
    // loops through mat1 and mat2 and performs element-wise subtraction
    for (int i = 0; i < mat1.rows; i++) {
        for (int j = 0; j < mat1.cols; j++) {
            difference.data[i][j] = mat1.data[i][j] - mat2.data[i][j];
        }
    }
    
    // returns difference
    return difference;
}

// dot product of two matrices
Matrix dot(Matrix mat1, Matrix mat2) {
    
    // ensure matrics are of compatible sizes
    if (mat1.cols != mat2.rows) {
        std::cout << "Error in Matrix Dot Product: incompatible sizes" << "\n";
        exit(1);
    }
    
    // create matrix to hold the dot product
    Matrix d_prod = Matrix_init(mat1.rows, mat2.cols, false);
    
    // for every row of matrix 1
    for (int k = 0; k < mat1.rows; k++) {
        
        // get dot product for every column in matrix 2
        for (int i = 0; i < mat2.cols; i++) {
            
            double sum = 0.0;
            
            // loop for every row value in a particular column
            for (int j = 0; j < mat2.rows; j++) {
                sum += mat1.data[k][j] * mat2.data[j][i];
            }
            // enter final computation for column into the result
            d_prod.data[k][i] = sum;
            
        }
    }
    
    // return dot product
    return d_prod;
} // dot()


// transpose of a matrix
Matrix transpose(Matrix mat) {
    
    // create transposed matrix
    Matrix T = Matrix_init(mat.cols, mat.rows, false);
    
    // copy over data values of previous matrix into new positions in transpose
    for (int i = 0; i < mat.rows; i++) {
        for (int j = 0; j < mat.cols; j++) {
            T.data[j][i] = mat.data[i][j];
        }
    }
    
    // return transposed matrix
    return T;
    
} // transpose()


// fills a matrix with a particular value, primarily for library testing purposes
void fill_matrix(Matrix &mat, double d) {
    for (int i = 0; i < mat.rows; i++) {
        for (int j = 0; j < mat.cols; j++) {
            mat.data[i][j] = d;
        }
    }
}
