//
//  dense.cpp
//  matrix
//
//  Created by Colin McGravey on 8/9/22.
//

#include "dense.hpp"
#include <stdio.h>
#include <iostream>
#include <vector>
#include <cmath>
#include "matrix.hpp"
#include "Layer.h"
using namespace std;

void Dense::print_outs() {
    for (int i = 0; i < lyr_output.rows; i++) {
        for (int j = 0; j < lyr_output.cols; j++) {
            std::cout << "[" << lyr_output.data[i][j] << "]";
        }
        std::cout << "\n";
    }
}

void Dense::view_params() {
    for (int i = 0; i < weights.rows; i++) {
        for (int j = 0; j < weights.cols; j++) {
            std::cout << "[" << weights.data[i][j] << "]";
        }
        std::cout << "\n";
    }
    std::cout << "Num Weights: " << weights.rows * weights.cols << "\n";
    
    for (int i = 0; i < biases.rows; i++) {
        for (int j = 0; j < biases.cols; j++) {
            std::cout << "[" << biases.data[i][j] << "]";
        }
        std::cout << "\n";
    }
    std::cout << "Num Biases: " << biases.rows * biases.cols << "\n";
}

Matrix Dense::forward_prop(Matrix &inputs) {
    lyr_input = inputs;
    
    Matrix d_prod = dot(lyr_input, weights);
    
    lyr_output = matrix_add(d_prod, biases);
    
    return lyr_output;
    
}

Matrix Dense::back_prop(Matrix &loss_grads, double lr) {
    Matrix wt_grads = dot(transpose(lyr_input), loss_grads);
    
    Matrix x_grads = dot(loss_grads, transpose(weights));
    
    weights = matrix_subtract(weights, sc_multiply(lr, wt_grads));
    biases = matrix_subtract(biases, sc_multiply(lr, loss_grads));
    
    return x_grads; 
}
