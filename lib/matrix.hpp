//
//  matrix.hpp
//  matrix
//
//  Created by Colin McGravey on 8/9/22.
//

#ifndef matrix_hpp
#define matrix_hpp

#include <stdio.h>
#include <iostream>
#include <vector>
#include <cmath>
#include <algorithm>
using namespace std;


// used to create matrices for weights and biases in the nn
double random_double();

// matrix struct representing a 2D matrix
struct Matrix {
    size_t cols;
    size_t rows;
    vector<vector<double> > data;
    
    Matrix& operator= (const Matrix& mat) {
        cols = mat.cols;
        rows = mat.rows;
        data = mat.data;
        
        return *this; 
    }
};

// initializes a Matrix with the specified height and width, if random flag is True, data values are random doubles
Matrix Matrix_init(size_t height, size_t width, bool random);

// scalar multiplacation of a matrix
Matrix sc_multiply(double c, Matrix mat1);

// Matrix addition
Matrix matrix_add(Matrix mat1, Matrix mat2);

// Matrix subtraction
Matrix matrix_subtract(Matrix mat1, Matrix mat2);

// dot product of two matrices
Matrix dot(Matrix mat1, Matrix mat2);

// returns transpose of a matrix
Matrix transpose(Matrix mat);

// fills a matrix with a particular value d, primarily for library testing purposes
void fill_matrix(Matrix &mat, double d); 


#endif /* matrix_hpp */
