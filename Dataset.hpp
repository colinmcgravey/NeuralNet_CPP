//
//  Dataset.hpp
//  matrix
//
//  Created by Colin McGravey on 8/10/22.
//

#ifndef Dataset_hpp
#define Dataset_hpp

#include <stdio.h>
#include <iostream>
#include <vector>
#include <cmath>
#include <algorithm>
#include <fstream>
#include "lib/matrix.hpp"
#include <sstream>
using namespace std;


class Dataset {
    
public:
    
    Dataset(string filename, int input_length, int classes) {
        read_file(filename, input_length);
        num_classes = classes; 
    }
    
    void read_file(string fname, int inp_length) {
        
        fstream fin;
        
        fin.open(fname, ios::in);
        
        if (fin.is_open() == false) {
            std::cout << "Error: File " << fname << " could not be opened" << "\n";
        }
        
        string line , word;
        size_t count = 0;
        
        while (!fin.eof()) {
            
            Matrix row = Matrix_init(1, 784, false);
            Matrix target = Matrix_init(1, 1, false);
            
            getline(fin, line);
            
            if (line == "") { break; }
            char lbl = line.at(0);
            int label = lbl - '0';
            target.data[0][0] = ((double)label);
            line.erase(0, 2);
            
            stringstream ss(line);
            
            for (int i = 0; i < inp_length; i++) {
                double pix;
                char temp;
                ss >> pix >> temp;
                row.data[0][i] = (pix / 255);
            }
            
            targets.push_back(target);
            inputs.push_back(row);
            count++;
            
        }
        
        std::cout << "Loaded " << count << " examples into dataset" << "\n";
        
        
    }
    
    Matrix get_row(size_t idx) {
        return inputs[idx];
    }
    
    Matrix get_lbl(size_t idx, bool encode) {
        if (!encode) {
            return targets[idx];
        }
        else {
            double num = targets[idx].data[0][0];
            Matrix lbl = Matrix_init(1, num_classes, false);
            for (int i = 0; i < num_classes; i++) {
                if (i == num) { lbl.data[0][i] = (1); }
                else { lbl.data[0][i] = (0); }
            }
            return lbl;
        }
    }
    
    size_t get_data_length() {
        return inputs.size(); 
    }
    
    
private:
    
    vector<Matrix> targets;
    vector<Matrix> inputs;
    
    int num_classes;
    
    
};

#endif /* Dataset_hpp */
