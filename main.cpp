//
//  main.cpp
//  matrix
//
//  Created by Colin McGravey on 8/8/22.
//

#include <iostream>
#include <stdio.h>
#include <vector>
#include <cmath>
#include <algorithm>
#include "lib/matrix.hpp"
#include "lib/dense.hpp"
#include "lib/model.hpp"
#include "Dataset.hpp"
using namespace std;






int main(int argc, const char * argv[]) {
    
    int input_shape = 784;
    int num_classes = 10;
    
    
    Dataset train_ds = Dataset("train.txt", input_shape, num_classes);
    
    Model model = Model(num_classes);
    
    model.add("Dense", 784, 256);
    model.add("RELu", 256, 256);
    model.add("Dense", 256, 64);
    model.add("RELu", 64, 64);
    model.add("Dense", 64, 10);
    model.summary();
    
    int epochs = 10;
    
    
    // training loop making use of the model.fit() function
    for (int i = 0; i < epochs; i++) {
        vector<double> epoch_loss;
        // fits the model to a segment of input data and passes the corresponding label to calculate loss
        for (int j = 0; j < 5000; j++) {
            Matrix example_img = train_ds.get_row(j);
            Matrix lbl_encode = train_ds.get_lbl(j, true);
            double loss = model.fit(example_img, lbl_encode, 0.0001);
            epoch_loss.push_back(loss);
        }
        
        // computes the average loss of all training examples in an epoch
        double avg_loss = model.epoch_end(epoch_loss);
        std::cout << "Epoch: " << i << " Loss: " << avg_loss << "\n";
    }
    
    
    
    return 0;
    
    
}
