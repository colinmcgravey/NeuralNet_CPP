# NeuralNet_CPP #

## Overview ##

This project implements a very simple feedforward neural network that uses 
logistic regression to make predictions on the MNIST dataset. In the current 
version, the *main.cpp* file is hard-coded to only train itself on the first 
5000 examples of the set. This was done for purpose of speeding up the runtime 
process and allowing for easier bug testing and fixing. 

## Objects ##


#### Matrices #### 

At this project's core is the *matrix.hpp* and *matrix.cpp* files. These files
contain the code for the Matrix struct, holding row and col variables, and a 
2D vector of data values. 

These files contain the necessary functions to perform addition, subtraction, 
scalar multiplacation, and matrix multiplacation of matrices, handled by 
*sc_multiply()*, *dot()*, *matrix_add()*, and *matrix_subtract()*. Also, the 
function *transpose()* returns the transposition of a given matrix. 

A picture demonstrating the layout of a Matrix struct is given below. 

![Matrix struct](rdme_imgs/matrix.png)


#### Layers ####

The Layer class is a pure abstract class containing four different functions and 
a constructor. This allows for users to build new layer types on top of this class, 
so long as they implement *forward_prop()*, *back_prop()*, *view_params()*, and 
*print_outs()* functions. 

The image below demonstrates the layout and necessary architecture for all classes 
that are built on top of this Layer class. 

![](rdme_imgs/Layer.png)



#### Dense and RELu ####

The Dense and RELu classes both inherit from the Layer class, and employ their own 
separate constructors to initialize the necessary parameters for forward and back 
propagation. However, due to the abstract nature of the Layer class, users will not 
have to create objects for each individual layer in their model. The layers can exist 
within a pointer to the Layer class. This greatly simplifies the implementation of the 
model class. 

A RELu layer should always be added to the model with the same input and output dimensions,
as there is no data reshaping occuring in these layers. Also, due to the nature of the 
model class implementation, this network allows only for Sequential model types, so the 
output of a previous layer is always the input for the next. 

In the model section of this README I will demonstrate how to add these layers to your 
model architecture. 


#### Model ####

The model class, contained in the *lib* folder, contains the functions necessary 
to assemble and fit a model to a matrix of input data. A model must be declared 
with one argument in the constructor, this argument being the number of outputs 
that your architecture will produce. Following this, one can add layers to their 
network using the *model.add()* function. 

This function takes three args, layer type and input and output dimensions. An 
example of the function being used is given below. 

![](rdme_imgs/modeladd.png)


Also, users can view all parameters and activations existing in their network at 
any time by calling the *model.summary()* function, which in turn, calls *view_params()* 
on each layer in the network. 


The function that makes up the heart of the model class is the *fit()* function, which 
takes a matrix of inputs, a matrix of target values, and a learning rate as arguments. 
This function will pass the inputs forward through each of the network layers, use the 
target values to calculate the network loss, and then back propagate the correct 
gradients through the layers using gradient descent. The learning rate arg is used to 
control the rate at which the model parameters are changed by their gradients. 
Finally, this function will return the loss calculated for the input example to be 
saved for future access. 


![](rdme_imgs/fit.png)


At the end of each epoch, the *model.epoch_end()* function can be used. Given a vector 
of losses for each training example, this function computes and returns the average 
loss for all examples in an epoch. 


#### Dataset ####

The Dataset class is not included in the lib folder as it is meant primarily to be 
customized by you, the users. This class can be tailor fitted to whatever dataset 
you would prefer to work with, although it does already have a constructor and 
*read_file()* function implemented. These can be changed to read in data in different 
formats or of different file types, but for now it serves the purpose of showing 
users its capability and how it should be used. 

Upon reading in data, the class will save each individual example in a Matrix, and 
each Matrix will be stored in a *vector<Matrix>* called dataset. The *get_row()* and 
*get_lbl()* functions, return the corresponding data and label at an index, and can be 
used to retrieve inputs and targets for your *model.fit()* function. 


## Contributions ## 

A large amount of the knowledge used to complete this project came from the article, 
[Neural Network from scratch in Python](https://towardsdatascience.com/math-neural-network-from-scratch-in-python-d6da9f29ce65), by Omar Aflak. This 
article is perfectly written, and made many extremely confusing concepts crystal 
clear for me. Omar I can't thank you enough. 

Please feel free to clone this repo and build your own network layers on top of the 
abstract Layer class, or use the layers that already exist to solve your own 
custom machine learning problem. Also, feel free to reach out and send me any 
questions or improvements to the code, I am interested to see what the community 
has to offer! Thanks for your time :)



